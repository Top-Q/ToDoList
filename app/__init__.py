from flask_restplus import Api
from .task_list_controller import api as task_list_api
from .task_controller import api as task_api
from .user_controller import api as user_api

api = Api(
    title='Todo List API',
    version='1.0',
    description='Todo List demo API',
)

api.add_namespace(user_api)
api.add_namespace(task_list_api)
api.add_namespace(task_api)