from flask import jsonify, request
from flask_restplus import Namespace, Resource
from modal.task_list import TaskList
from modal.data import users, save

api = Namespace('todolist', description='ToDo List')


@api.route("/<string:user>/<string:task_list_name>")
class TaskListController(Resource):

    def put(self, user, task_list_name):
        task_list = TaskList(task_list_name)
        users[user].create_task_list(task_list)
        save()
        return jsonify(task_list.serialize())

    def get(self, user, task_list_name):
        return jsonify(users[user].get_task_list(task_list_name).serialize())

    def delete(self, user, task_list_name):
        task_list = users[user].get_task_list(task_list_name)
        save()
        return "deleted " + task_list_name

    def post(self, user, task_list_name):
        task_list = users[user].get_task_list(task_list_name)
        task_list.set_description(request.form["description"])
        save()
        return jsonify(task_list.serialize())