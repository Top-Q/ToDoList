from flask import request, jsonify
from flask_restplus import Namespace, Resource
from modal.user import User
from modal.data import users, save

api = Namespace('todolist', description='ToDo List')

@api.route("/user/<string:user_id>")
class UserController(Resource):

    def put(self, user_id):
        user = User(user_id, request.form['password'], request.form['first_name'], request.form['last_name'],
                    request.form['email'])
        users[user_id] = user
        save()
        return jsonify(user.serialize())

    def get(self, user_id):
        return jsonify(users[user_id].serialize())

    def delete(self, user_id):
        del users[user_id]
        save()
        return "deleted " + user_id

    def post(self, user_id):
        user = users[user_id]
        user.set_user_password(request.form['password'])
        user.set_user_name(request.form['first_name'])
        user.set_user_last_name(request.form['last_name'])
        user.set_user_email(request.form['email'])
        save()
        return jsonify(user.serialize())

