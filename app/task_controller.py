from flask import jsonify, request
from flask_restplus import Namespace, Resource
from modal.task import Task
from modal.data import users, save

api = Namespace('todolist', description='ToDo List')


@api.route("/<string:user>/<string:task_list_name>/<string:task_name>")
class TaskController(Resource):

    def put(self, user, task_list_name, task_name):
        task = Task(task_name)
        users[user].get_task_list(task_list_name).create_task(task)
        save()
        return jsonify(task.serialize())

    def get(self, user, task_list_name, task_name):
        return jsonify(users[user].get_task_list(task_list_name).get_task(task_name).serialize())

    def post(self, user, task_list_name, task_name):
        task = users[user].get_task_list(task_list_name).get_task(task_name)
        if request.form["field"] == "description":
            task.set_description(request.form["description"])
        elif request.form["field"] == "scheduled":
            task.set_scheduled(request.form["scheduled"])
        elif request.form["field"] == "close":
            task.close_task()
        save()
        return jsonify(task.serialize())

    def delete(self, user, task_list_name, task_name):
        task = users[user].get_task_list(task_list_name).get_task(task_name)
        del task
        save()
        return "deleted " + task_name