from setuptools import setup

setup(
    name='ToDoList',
    packages=['app'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)