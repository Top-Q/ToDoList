from modal.file_writer import load_object
from modal.file_writer import save_object


users = load_object('data.pkl')
if users is None:
    users = {}

def save():
    save_object(users, 'data.pkl')