class User:

    def __init__(self, user_id, password, first_name, last_name, email):
        self._user_id = user_id
        self._password = password
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._list_task_list = {}

    def create_task_list(self, task_list):
        self._list_task_list[task_list.get_task_list_name()] = task_list

    def delete_task_list(self, task_list):
        del self._list_task_list[task_list.get_task_name()]

    def get_task_list(self, task_list_name):
        return self._list_task_list[task_list_name]

    def get_user_id(self):
        return self._user_id

    def get_user_email(self):
        return self._email

    def get_user_password(self):
        return self._password

    def get_user_name(self):
        return self._first_name

    def get_user_last_name(self):
        return self._last_name

    def set_user_email(self, email):
        self._email = email

    def set_user_password(self, password):
        self._password = password

    def set_user_name(self, first_name):
        self._first_name = first_name

    def set_user_last_name(self, last_name):
        self._last_name = last_name

    def serialize(self):
        list_task_list = {}
        for task_list in self._list_task_list.values():
            list_task_list[task_list.get_task_list_name()] = task_list.serialize()
        return {
            'user_id': self._user_id,
            'password': self._password,
            'first_name': self._first_name,
            'last_name': self._last_name,
            'email': self._email,
            'list_task_list': list_task_list
        }