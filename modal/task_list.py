class TaskList:

    def __init__(self, name):
        self._name = name
        self._description = ""
        self._task_list = {}

    def set_description(self, description):
        self._description = description

    def get_description(self):
        return self._description

    def create_task(self, task):
        self._task_list[task.get_task_name()] = task

    def get_task(self, task_name):
        return self._task_list[task_name]

    def add_task(self, task):
        self._task_list.append(task)

    def get_task_list_name(self):
        return self._name

    def serialize(self):
        task_list = {}
        for task in self._task_list.values():
            task_list[task.get_task_name()] = task.serialize()
        return {
            'name': self._name,
            'description': self._description,
            'task_list': task_list
        }