from aenum import Enum


class Status(Enum):
    open = 0
    completed = 1

    def __str__(self):
        return self.name

    def serialize(self):
        return self.__str__()

