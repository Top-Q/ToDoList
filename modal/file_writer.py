import cPickle


def load_object(filename):
    try:
        with open(filename, 'rb') as input:
            try:
                return cPickle.load(input)
            except EOFError:
                return None
    except IOError:
        open(filename, 'wb')
        return None

def save_object(obj, filename):
    with open(filename, 'wb') as output:
        cPickle.dump(obj, output, cPickle.HIGHEST_PROTOCOL)