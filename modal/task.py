from modal.status import Status


class Task:

    def __init__(self, name):
        self._name = name
        self._description = ""
        self._status = Status.open
        self._scheduled = ""

    def set_description(self, description):
        self._description = description

    def get_description(self):
        return self._description

    def set_scheduled(self, scheduled):
        self._scheduled = scheduled

    def get_scheduled(self):
        return self._scheduled

    def get_task_name(self):
        return self._name

    def close_task(self):
        self._status = Status.completed

    def get_status(self):
        return self._status

    def serialize(self):
        return {
            'name' : self._name,
            'description' : self._description,
            'status' : self._status.serialize(),
            'scheduled' : self.get_scheduled()
        }