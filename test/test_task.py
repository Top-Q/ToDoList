import requests
from modal.task import Task

class TestTask(object):

    def test_create_task(self):
        user = "user1"
        task_list_name = "mytasklist"
        task = Task('mytask')
        r = requests.put("http://127.0.0.1:5000/todolist/" + user + "/" + task_list_name + "/" + task.get_task_name())
        assert r.status_code == 200
        assert r.json() == task.serialize()

    def test_get_task(self):
        user = "user1"
        task_list_name = "mytasklist"
        task = Task('mytask')
        r = requests.put("http://127.0.0.1:5000/todolist/" + user + "/" + task_list_name + "/" + task.get_task_name())
        assert r.status_code == 200
        r = requests.get("http://127.0.0.1:5000/todolist/" + user + "/" + task_list_name + "/" + task.get_task_name())
        assert r.json() == task.serialize()

    def test_update(self):
        user = "user1"
        task_list_name = "mytasklist"
        task = Task('mytask')
        r = requests.put("http://127.0.0.1:5000/todolist/" + user + "/" + task_list_name + "/" + task.get_task_name())
        assert r.status_code == 200
        task.set_description("new description")
        r = requests.post("http://127.0.0.1:5000/todolist/" + user + "/" + task_list_name + "/" + task.get_task_name(),
                          data={"field" : "description", "description" : task.get_description()})
        assert r.status_code == 200
        assert r.json() == task.serialize()
        task.set_scheduled("2016-10-10")
        r = requests.post("http://127.0.0.1:5000/todolist/" + user + "/" + task_list_name + "/" + task.get_task_name(),
                          data={"field": "scheduled", "scheduled": task.get_scheduled()})
        assert r.status_code == 200
        assert r.json() == task.serialize()
        task.close_task()
        r = requests.post("http://127.0.0.1:5000/todolist/" + user + "/" + task_list_name + "/" + task.get_task_name(),
                          data={"field": "close"})
        assert r.status_code == 200
        assert r.json() == task.serialize()

    def test_delete_task(self):
        user = "user1"
        task_list_name = "mytasklist"
        task = Task('mytask')
        r = requests.put("http://127.0.0.1:5000/todolist/" + user + "/" + task_list_name + "/" + task.get_task_name())
        assert r.status_code == 200
        r = requests.delete("http://127.0.0.1:5000/todolist/" + user + "/" + task_list_name + "/" + task.get_task_name())
        assert r.status_code == 200
        assert r.json() == 'deleted ' + task.get_task_name()
