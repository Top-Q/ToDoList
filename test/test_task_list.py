import requests
from modal.task_list import TaskList


class TestTaskList(object):

    def test_task_list_creation(self):
        user = "user1"
        task_list = TaskList('mytasklist')
        r = requests.put("http://127.0.0.1:5000/todolist/" + user + "/" + task_list.get_task_list_name())
        assert r.status_code == 200
        assert r.json() == task_list.serialize()

    def test_task_list_get(self):
        user = "user1"
        task_list = TaskList('mytasklist')
        r = requests.put("http://127.0.0.1:5000/todolist/" + user + "/" + task_list.get_task_list_name())
        assert r.status_code == 200
        r = requests.get("http://127.0.0.1:5000/todolist/" + user + "/" + task_list.get_task_list_name())
        assert r.status_code == 200
        assert r.json() == task_list.serialize()

    def test_task_list_update(self):
        user = "user1"
        task_list = TaskList('mytasklist')
        r = requests.put("http://127.0.0.1:5000/todolist/" + user + "/" + task_list.get_task_list_name())
        assert r.status_code == 200
        task_list.set_description("my description")
        r = requests.post("http://127.0.0.1:5000/todolist/" + user + "/" + task_list.get_task_list_name(), data={'description': task_list.get_description()})
        assert r.status_code == 200
        assert r.json() == task_list.serialize()

    def test_task_list_remove(self):
        user = "user1"
        task_list = TaskList('mytasklist')
        r = requests.put("http://127.0.0.1:5000/todolist/" + user + "/" + task_list.get_task_list_name())
        assert r.status_code == 200
        r = requests.delete("http://127.0.0.1:5000/todolist/" + user + "/" + task_list.get_task_list_name(), data={'description': task_list.get_description()})
        assert r.status_code == 200
        assert r.json() == 'deleted ' + task_list.get_task_list_name()