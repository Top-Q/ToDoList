import requests
from modal.user import User


class TestUsers(object):

    def test_user_creation(self):
        user = User('max123', '123', 'max', 'ch', 'abc@abc.com')
        r = requests.put('http://127.0.0.1:5000/todolist/user/' + user.get_user_id(),
                         data={'password': user.get_user_password(), 'first_name': user.get_user_name(),
                               'last_name': user.get_user_last_name(), 'email': user.get_user_email()})
        assert r.status_code == 200
        assert r.json() == user.serialize()

    def test_get_user(self):
        user = User('max123', '123', 'max', 'ch', 'abc@abc.com')
        r = requests.put('http://127.0.0.1:5000/todolist/user/' + user.get_user_id(),
                         data={'password': user.get_user_password(), 'first_name': user.get_user_name(),
                               'last_name': user.get_user_last_name(), 'email': user.get_user_email()})
        assert r.status_code == 200
        r = requests.get('http://127.0.0.1:5000/todolist/user/' + user.get_user_id())
        assert r.status_code == 200
        assert r.json() == user.serialize()

    def test_user_update(self):
        user = User('max123', '123', 'max', 'ch', 'abc@abc.com')
        r = requests.put('http://127.0.0.1:5000/todolist/user/' + user.get_user_id(),
                         data={'password': user.get_user_password(), 'first_name': user.get_user_name(),
                               'last_name': user.get_user_last_name(), 'email': user.get_user_email()})
        assert r.status_code == 200
        user.set_user_password('111')
        user.set_user_name('aaa')
        user.set_user_last_name('bbb')
        user.set_user_email('aaa@aaa.com')
        r = requests.post('http://127.0.0.1:5000/todolist/user/' + user.get_user_id(),
                         data={'password': user.get_user_password(), 'first_name': user.get_user_name(),
                               'last_name': user.get_user_last_name(), 'email': user.get_user_email()})
        assert r.status_code == 200
        assert r.json() == user.serialize()

    def test_user_delete(self):
        user = User('max123', '123', 'max', 'ch', 'abc@abc.com')
        r = requests.put('http://127.0.0.1:5000/todolist/user/' + user.get_user_id(),
                         data={'password': user.get_user_password(), 'first_name': user.get_user_name(),
                               'last_name': user.get_user_last_name(), 'email': user.get_user_email()})
        assert r.status_code == 200
        r = requests.delete('http://127.0.0.1:5000/todolist/user/' + user.get_user_id())
        assert r.status_code == 200
        assert r.json() == 'deleted ' + user.get_user_id()